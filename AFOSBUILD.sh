rm -rf /opt/ANDRAX/wacker

cp defconfig wpa_supplicant-2.10/wpa_supplicant/.config 

patch -p1 < wpa_supplicant.patch

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Patch... PASS!"
else
  # houston we have a problem
  exit 1
fi

WORKDIR=$(pwd)

cd wpa_supplicant-2.10/wpa_supplicant

make -j1

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make wpa_supplicant... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd $WORKDIR

cp -Rf $(pwd) /opt/ANDRAX/wacker

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
